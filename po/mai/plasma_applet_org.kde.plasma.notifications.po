# translation of plasma_applet_notifications.po to Maithili
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Rajesh Ranjan <rajesh672@gmail.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_notifications\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-01 01:39+0000\n"
"PO-Revision-Date: 2010-09-24 16:02+0530\n"
"Last-Translator: Rajesh Ranjan <rajesh672@gmail.com>\n"
"Language-Team: Maithili <bhashaghar@googlegroups.com>\n"
"Language: mai\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"\n"
"X-Generator: KBabel 1.11.4\n"

#: fileinfo.cpp:170
#, kde-format
msgid "Open with %1"
msgstr ""

#: fileinfo.cpp:174
#, kde-format
msgid "Open with…"
msgstr ""

#: filemenu.cpp:117 package/contents/ui/JobItem.qml:268
#, kde-format
msgid "Open Containing Folder"
msgstr ""

#: filemenu.cpp:131
#, kde-format
msgid "&Copy"
msgstr ""

#: filemenu.cpp:139
#, kde-format
msgctxt "@action:incontextmenu"
msgid "Copy Location"
msgstr ""

#: filemenu.cpp:195
#, kde-format
msgid "Properties"
msgstr ""

#: globalshortcuts.cpp:23
#, kde-format
msgid "Toggle do not disturb"
msgstr ""

#: globalshortcuts.cpp:42
#, fuzzy, kde-format
#| msgid "Notifications"
msgctxt "OSD popup, keep short"
msgid "Notifications Off"
msgstr "सूचनासभ"

#: globalshortcuts.cpp:43
#, fuzzy, kde-format
#| msgid "Notifications"
msgctxt "OSD popup, keep short"
msgid "Notifications On"
msgstr "सूचनासभ"

#: package/contents/ui/EditContextMenu.qml:32
#, kde-format
msgctxt "@action:inmenu"
msgid "Copy Link Address"
msgstr ""

#: package/contents/ui/EditContextMenu.qml:44
#, kde-format
msgctxt "@action:inmenu"
msgid "Copy"
msgstr ""

#: package/contents/ui/EditContextMenu.qml:61
#, kde-format
msgctxt "@action:inmenu"
msgid "Select All"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:69
#, kde-format
msgid "Do not disturb"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:111
#, kde-format
msgid "For 1 hour"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:116
#, kde-format
msgid "For 4 hours"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:125
#, kde-format
msgid "Until this evening"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:135
#, kde-format
msgid "Until tomorrow morning"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:147
#, kde-format
msgid "Until Monday"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:154
#, kde-format
msgid "Until manually disabled"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:204
#, kde-format
msgctxt "Do not disturb until date"
msgid "Automatically ends: %1"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:216
#, kde-format
msgctxt "Do not disturb until app has finished (reason)"
msgid "While %1 is active (%2)"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:218
#, kde-format
msgctxt "Do not disturb until app has finished"
msgid "While %1 is active"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:224
#, kde-format
msgctxt "Do not disturb because external mirrored screens connected"
msgid "Screens are mirrored"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:432
#, kde-format
msgid "Close Group"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:577
#, kde-format
msgid "Show Fewer"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:579
#, kde-format
msgctxt "Expand to show n more notifications"
msgid "Show %1 More"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:620
#: package/contents/ui/main.qml:105
#, fuzzy, kde-format
#| msgid "Notifications"
msgid "No unread notifications"
msgstr "सूचनासभ"

#: package/contents/ui/FullRepresentation.qml:635
#: package/contents/ui/main.qml:79
#, fuzzy, kde-format
#| msgid "No notifications and no jobs"
msgid "Notification service not available"
msgstr "सूचना नहि आओर काज नहि"

#: package/contents/ui/FullRepresentation.qml:637
#, kde-format
msgctxt "Vendor and product name"
msgid "Notifications are currently provided by '%1 %2'"
msgstr ""

#: package/contents/ui/JobDetails.qml:34
#, kde-format
msgctxt "Row description, e.g. Source"
msgid "%1:"
msgstr ""

#: package/contents/ui/JobDetails.qml:105
#, kde-format
msgctxt "How many bytes have been copied"
msgid "%2 of %1"
msgstr ""

#: package/contents/ui/JobDetails.qml:109
#, kde-format
msgctxt "How many files have been copied"
msgid "%2 of %1 file"
msgid_plural "%2 of %1 files"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/JobDetails.qml:112
#, kde-format
msgctxt "How many dirs have been copied"
msgid "%2 of %1 folder"
msgid_plural "%2 of %1 folders"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/JobDetails.qml:115
#, kde-format
msgctxt "How many items (that includes files and dirs) have been copied"
msgid "%2 of %1 item"
msgid_plural "%2 of %1 items"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/JobDetails.qml:123
#, kde-format
msgid "%1 file"
msgid_plural "%1 files"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/JobDetails.qml:125
#, kde-format
msgid "%1 folder"
msgid_plural "%1 folders"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/JobDetails.qml:127
#, kde-format
msgid "%1 item"
msgid_plural "%1 items"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/JobDetails.qml:144
#, kde-format
msgctxt "Bytes per second"
msgid "%1/s"
msgstr ""

#: package/contents/ui/JobItem.qml:164
#, kde-format
msgctxt "Percentage of a job"
msgid "%1%"
msgstr ""

#: package/contents/ui/JobItem.qml:178
#, fuzzy, kde-format
#| msgid "Pause job"
msgctxt "Pause running job"
msgid "Pause"
msgstr "काज ठहराबू"

#: package/contents/ui/JobItem.qml:188
#, fuzzy, kde-format
#| msgid "Cancel job"
msgctxt "Cancel running job"
msgid "Cancel"
msgstr "काज रद करू"

#: package/contents/ui/JobItem.qml:199
#, kde-format
msgctxt "A button tooltip; hides item details"
msgid "Hide Details"
msgstr ""

#: package/contents/ui/JobItem.qml:200
#, kde-format
msgctxt "A button tooltip; expands the item to show details"
msgid "Show Details"
msgstr ""

#: package/contents/ui/JobItem.qml:232
#: package/contents/ui/ThumbnailStrip.qml:169
#, kde-format
msgid "More Options…"
msgstr ""

#: package/contents/ui/JobItem.qml:260
#, kde-format
msgid "Open"
msgstr ""

#: package/contents/ui/main.qml:63
#, kde-format
msgid "%1 running job"
msgid_plural "%1 running jobs"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/main.qml:67
#, kde-format
msgctxt "Job title (percentage)"
msgid "%1 (%2%)"
msgstr ""

#: package/contents/ui/main.qml:72
#, kde-format
msgid "%1 running job (%2%)"
msgid_plural "%1 running jobs (%2%)"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/main.qml:85
#, fuzzy, kde-format
#| msgid "Notifications"
msgid "%1 unread notification"
msgid_plural "%1 unread notifications"
msgstr[0] "सूचनासभ"
msgstr[1] "सूचनासभ"

#: package/contents/ui/main.qml:98
#, kde-format
msgid "Do not disturb until %1; middle-click to exit now"
msgstr ""

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Do not disturb mode active; middle-click to exit"
msgstr ""

#: package/contents/ui/main.qml:107
#, kde-format
msgid "Middle-click to enter do not disturb mode"
msgstr ""

#: package/contents/ui/main.qml:229
#, fuzzy, kde-format
#| msgid "Notifications"
msgid "Clear All Notifications"
msgstr "सूचनासभ"

#: package/contents/ui/main.qml:248
#, kde-format
msgid "&Configure Event Notifications and Actions…"
msgstr ""

#: package/contents/ui/NotificationHeader.qml:120
#, kde-format
msgctxt "Notification was added less than a minute ago, keep short"
msgid "Just now"
msgstr ""

#: package/contents/ui/NotificationHeader.qml:126
#, kde-format
msgctxt "Notification was added minutes ago, keep short"
msgid "%1 min ago"
msgid_plural "%1 min ago"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/NotificationHeader.qml:160
#, kde-format
msgctxt "seconds remaining, keep short"
msgid "%1 s remaining"
msgid_plural "%1 s remaining"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/NotificationHeader.qml:164
#, kde-format
msgctxt "minutes remaining, keep short"
msgid "%1 min remaining"
msgid_plural "%1 min remaining"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/NotificationHeader.qml:169
#, kde-format
msgctxt "hours remaining, keep short"
msgid "%1 h remaining"
msgid_plural "%1 h remaining"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/NotificationHeader.qml:189
#, kde-format
msgid "Configure"
msgstr ""

#: package/contents/ui/NotificationHeader.qml:206
#, kde-format
msgctxt "Opposite of minimize"
msgid "Restore"
msgstr ""

#: package/contents/ui/NotificationHeader.qml:207
#, kde-format
msgid "Minimize"
msgstr ""

#: package/contents/ui/NotificationHeader.qml:230
#, kde-format
msgid "Close"
msgstr ""

#: package/contents/ui/NotificationItem.qml:186
#, fuzzy, kde-format
#| msgid "Paused"
msgctxt "Job name, e.g. Copying is paused"
msgid "%1 (Paused)"
msgstr "ठहरू"

#: package/contents/ui/NotificationItem.qml:191
#, fuzzy, kde-format
#| msgid "Paused"
msgctxt "Job name, e.g. Copying has failed"
msgid "%1 (Failed)"
msgstr "ठहरू"

#: package/contents/ui/NotificationItem.qml:193
#, kde-format
msgid "Job Failed"
msgstr ""

#: package/contents/ui/NotificationItem.qml:197
#, fuzzy, kde-format
#| msgid "Paused"
msgctxt "Job name, e.g. Copying has finished"
msgid "%1 (Finished)"
msgstr "ठहरू"

#: package/contents/ui/NotificationItem.qml:199
#, kde-format
msgid "Job Finished"
msgstr ""

#: package/contents/ui/NotificationItem.qml:346
#, kde-format
msgctxt "Reply to message"
msgid "Reply"
msgstr ""

#: package/contents/ui/NotificationReplyField.qml:36
#, kde-format
msgctxt "Text field placeholder"
msgid "Type a reply…"
msgstr ""

#: package/contents/ui/NotificationReplyField.qml:56
#, kde-format
msgctxt "@action:button"
msgid "Send"
msgstr ""

#, fuzzy
#~| msgid "Resume job"
#~ msgctxt "Resume paused job"
#~ msgid "Resume"
#~ msgstr "काज पुनरारंभ करू"

#, fuzzy
#~| msgid "Notifications"
#~ msgid "No unread notifications."
#~ msgstr "सूचनासभ"

#~ msgid "Information"
#~ msgstr "सूचना"

#, fuzzy
#~| msgid "Show notifications"
#~ msgid "Show application and system notifications"
#~ msgstr "सूचना देखाबू"

#, fuzzy
#~| msgid "Show notifications"
#~ msgid "Show a history of notifications"
#~ msgstr "सूचना देखाबू"

#, fuzzy
#~| msgid "No notifications and no jobs"
#~ msgid "No notifications or jobs"
#~ msgstr "सूचना नहि आओर काज नहि"

#, fuzzy
#~| msgid "Show notifications"
#~ msgid "&Application notifications:"
#~ msgstr "सूचना देखाबू"

#, fuzzy
#~| msgctxt "Show all  notifications"
#~| msgid "All"
#~ msgid "All"
#~ msgstr "सभ"

#~ msgid "More"
#~ msgstr "बेसी"

#~ msgid "Less"
#~ msgstr "कम"

#~ msgid "Notification from %1"
#~ msgstr "अधिसूचना %1"

#~ msgid "Popup"
#~ msgstr "पाप अप"
