# translation of plasma_applet_devicenotifier.po to Telugu
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Krishna Babu K <kkrothap@redhat.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_devicenotifier\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-02 01:37+0000\n"
"PO-Revision-Date: 2009-01-16 22:16+0530\n"
"Last-Translator: Krishna Babu K <kkrothap@redhat.com>\n"
"Language-Team: Telugu <en@li.org>\n"
"Language: te\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: KBabel 1.11.4\n"

#: package/contents/ui/DeviceItem.qml:189
#, kde-format
msgctxt "@info:status Free disk space"
msgid "%1 free of %2"
msgstr ""

#: package/contents/ui/DeviceItem.qml:193
#, kde-format
msgctxt ""
"Accessing is a less technical word for Mounting; translation should be short "
"and mean 'Currently mounting this device'"
msgid "Accessing…"
msgstr ""

#: package/contents/ui/DeviceItem.qml:196
#, kde-format
msgctxt ""
"Removing is a less technical word for Unmounting; translation should be "
"short and mean 'Currently unmounting this device'"
msgid "Removing…"
msgstr ""

#: package/contents/ui/DeviceItem.qml:199
#, kde-format
msgid "Don't unplug yet! Files are still being transferred…"
msgstr ""

#: package/contents/ui/DeviceItem.qml:230
#, kde-format
msgid "Open in File Manager"
msgstr ""

#: package/contents/ui/DeviceItem.qml:233
#, kde-format
msgid "Mount and Open"
msgstr ""

#: package/contents/ui/DeviceItem.qml:235
#, kde-format
msgid "Eject"
msgstr ""

#: package/contents/ui/DeviceItem.qml:237
#, kde-format
msgid "Safely remove"
msgstr ""

#: package/contents/ui/DeviceItem.qml:279
#, kde-format
msgid "Mount"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:43
#: package/contents/ui/main.qml:252
#, kde-format
msgid "Remove All"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:44
#, kde-format
msgid "Click to safely remove all devices"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:185
#, kde-format
msgid "No removable devices attached"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:185
#, kde-format
msgid "No disks available"
msgstr ""

#: package/contents/ui/main.qml:56
#, kde-format
msgid "Most Recent Device"
msgstr ""

#: package/contents/ui/main.qml:56
#, kde-format
msgid "No Devices Available"
msgstr ""

#: package/contents/ui/main.qml:234
#, kde-format
msgctxt "Open auto mounter kcm"
msgid "Configure Removable Devices…"
msgstr ""

#: package/contents/ui/main.qml:260
#, kde-format
msgid "Removable Devices"
msgstr ""

#: package/contents/ui/main.qml:275
#, kde-format
msgid "Non Removable Devices"
msgstr ""

#: package/contents/ui/main.qml:290
#, kde-format
msgid "All Devices"
msgstr ""

#: package/contents/ui/main.qml:307
#, kde-format
msgid "Show popup when new device is plugged in"
msgstr ""
