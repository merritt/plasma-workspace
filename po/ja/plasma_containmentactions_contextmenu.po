# Translation of plasma_containmentactions_contextmenu into Japanese.
# Yukiko Bando <ybando@k6.dion.ne.jp>, 2009.
# Tomohiro Hyakutake <tomhioo@outlook.jp>, 2019.
# Ryuichi Yamada <ryuichi_ya220@outlook.jp>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma_containmentactions_contextmenu\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-01 01:39+0000\n"
"PO-Revision-Date: 2023-02-20 21:23+0900\n"
"Last-Translator: Ryuichi Yamada <ryuichi_ya220@outlook.jp>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Lokalize 22.12.2\n"

#: menu.cpp:100
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Show KRunner"
msgstr "KRunner を表示"

#: menu.cpp:105
#, kde-format
msgid "Open Terminal"
msgstr "ターミナルを開く"

#: menu.cpp:109
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Lock Screen"
msgstr "スクリーンをロック"

#: menu.cpp:118
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Leave…"
msgstr "終了..."

#: menu.cpp:127
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Configure Display Settings…"
msgstr "ディスプレイを設定..."

#: menu.cpp:283
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Configure Contextual Menu Plugin"
msgstr "コンテキストメニュープラグインの設定"

#: menu.cpp:293
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "[Other Actions]"
msgstr "[他のアクション]"

#: menu.cpp:296
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Wallpaper Actions"
msgstr "壁紙アクション"

#: menu.cpp:300
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "[Separator]"
msgstr "[セパレータ]"
