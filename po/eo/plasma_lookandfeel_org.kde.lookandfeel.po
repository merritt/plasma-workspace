# translation of plasma_lookandfeel_org.kde.lookandfeel.pot to esperanto
# Copyright (C) 2014 Free Software Foundation, Inc.
# This file is distributed under the same license as the plasma-workspace package.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
# Minuskloj: ĉ ĝ ĵ ĥ ŝ ŭ   Majuskloj: Ĉ Ĝ Ĵ Ĥ Ŝ Ŭ
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-08 01:36+0000\n"
"PO-Revision-Date: 2023-11-22 06:38+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: Esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: translate-po (https://github.com/zcribe/translate-po)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../sddm-theme/KeyboardButton.qml:19
#, kde-format
msgid "Keyboard Layout: %1"
msgstr "Klavara aranĝo: %1"

#: ../sddm-theme/Login.qml:85
#, kde-format
msgid "Username"
msgstr "Uzantnomo"

#: ../sddm-theme/Login.qml:102 contents/lockscreen/MainBlock.qml:64
#, kde-format
msgid "Password"
msgstr "Pasvorto"

#: ../sddm-theme/Login.qml:144 ../sddm-theme/Login.qml:150
#, kde-format
msgid "Log In"
msgstr "Ensaluti"

#: ../sddm-theme/Main.qml:201 contents/lockscreen/LockScreenUi.qml:277
#, kde-format
msgid "Caps Lock is on"
msgstr "Majskulo estas aktiva"

#: ../sddm-theme/Main.qml:213 ../sddm-theme/Main.qml:357
#: contents/logout/Logout.qml:167
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Dormi"

#: ../sddm-theme/Main.qml:220 ../sddm-theme/Main.qml:364
#: contents/logout/Logout.qml:188 contents/logout/Logout.qml:199
#, kde-format
msgid "Restart"
msgstr "Rekomenci"

#: ../sddm-theme/Main.qml:227 ../sddm-theme/Main.qml:371
#: contents/logout/Logout.qml:212
#, kde-format
msgid "Shut Down"
msgstr "Fermi sistemon"

#: ../sddm-theme/Main.qml:234
#, kde-format
msgctxt "For switching to a username and password prompt"
msgid "Other…"
msgstr "Alia…"

#: ../sddm-theme/Main.qml:343
#, kde-format
msgid "Type in Username and Password"
msgstr "Tajpu Uzantnomon kaj Pasvorton"

#: ../sddm-theme/Main.qml:378
#, kde-format
msgid "List Users"
msgstr "Listo de Uzantoj"

#: ../sddm-theme/Main.qml:453 contents/lockscreen/LockScreenUi.qml:364
#, kde-format
msgctxt "Button to show/hide virtual keyboard"
msgid "Virtual Keyboard"
msgstr "Virtuala Klavaro"

#: ../sddm-theme/Main.qml:527
#, kde-format
msgid "Login Failed"
msgstr "Ensaluto malsukcesis"

#: ../sddm-theme/SessionButton.qml:18
#, kde-format
msgid "Desktop Session: %1"
msgstr "Labortabla Sesio: %1"

#: contents/lockscreen/config.qml:19
#, kde-format
msgctxt "@title: group"
msgid "Clock:"
msgstr "Horloĝo:"

#: contents/lockscreen/config.qml:22
#, kde-format
msgctxt "@option:check"
msgid "Keep visible when unlocking prompt disappears"
msgstr "Konservi videbla kiam malŝlosa prompto malaperas"

#: contents/lockscreen/config.qml:33
#, kde-format
msgctxt "@title: group"
msgid "Media controls:"
msgstr "Plurmediaj regiloj:"

#: contents/lockscreen/config.qml:36
#, kde-format
msgctxt "@option:check"
msgid "Show under unlocking prompt"
msgstr "Montri sub malŝlosa prompto"

#: contents/lockscreen/LockScreenUi.qml:50
#, kde-format
msgid "Unlocking failed"
msgstr "Malŝloso malsukcesis"

#: contents/lockscreen/LockScreenUi.qml:291
#, kde-format
msgid "Sleep"
msgstr "Dormi"

#: contents/lockscreen/LockScreenUi.qml:297 contents/logout/Logout.qml:177
#, kde-format
msgid "Hibernate"
msgstr "Pasivumigi"

#: contents/lockscreen/LockScreenUi.qml:303
#, kde-format
msgid "Switch User"
msgstr "Ŝanĝi Uzanton"

#: contents/lockscreen/LockScreenUi.qml:388
#, kde-format
msgctxt "Button to change keyboard layout"
msgid "Switch layout"
msgstr "Ŝanĝi aranĝon"

#: contents/lockscreen/MainBlock.qml:107
#: contents/lockscreen/NoPasswordUnlock.qml:17
#, kde-format
msgid "Unlock"
msgstr "Malŝlosi"

#: contents/lockscreen/MainBlock.qml:155
#, kde-format
msgid "(or scan your fingerprint on the reader)"
msgstr "(aŭ skanu vian fingrospuron sur la legilo)"

#: contents/lockscreen/MainBlock.qml:159
#, kde-format
msgid "(or scan your smartcard)"
msgstr "(aŭ skanu vian inteligentan memorkarton)"

#: contents/lockscreen/MediaControls.qml:59
#, kde-format
msgid "No title"
msgstr "Neniu titolo"

#: contents/lockscreen/MediaControls.qml:60
#, kde-format
msgid "No media playing"
msgstr "Neniu aŭdvidaĵo ludanta"

#: contents/lockscreen/MediaControls.qml:88
#, kde-format
msgid "Previous track"
msgstr "Antaŭa trako"

#: contents/lockscreen/MediaControls.qml:100
#, kde-format
msgid "Play or Pause media"
msgstr "Ludi aŭ Paŭzi aŭdvidaĵon"

#: contents/lockscreen/MediaControls.qml:110
#, kde-format
msgid "Next track"
msgstr "Sekva aŭtoveturejo"

#: contents/logout/Logout.qml:142
#, kde-format
msgid ""
"One other user is currently logged in. If the computer is shut down or "
"restarted, that user may lose work."
msgid_plural ""
"%1 other users are currently logged in. If the computer is shut down or "
"restarted, those users may lose work."
msgstr[0] ""
"Unu alia uzanto estas aktuale ensalutita. Se la komputilo estas malŝaltita "
"aŭ restartigita, tiu uzanto eble perdos laboron."
msgstr[1] ""
"%1 aliaj uzantoj estas aktuale ensalutitaj. Se la komputilo estas malŝaltita "
"aŭ restartigita, tiuj uzantoj eble perdos laboron."

#: contents/logout/Logout.qml:156
#, kde-format
msgid "When restarted, the computer will enter the firmware setup screen."
msgstr "Rekomencinte, la komputilo eniros la ekranon de agordo de firmware."

#: contents/logout/Logout.qml:187
#, kde-format
msgctxt "@action:button Keep short"
msgid "Install Updates & Restart"
msgstr "Instali Ĝisdatigojn & Rekomenci"

#: contents/logout/Logout.qml:223
#, kde-format
msgid "Log Out"
msgstr "Elsaluti"

#: contents/logout/Logout.qml:247
#, kde-format
msgid "Installing software updates and restarting in 1 second"
msgid_plural "Installing software updates and restarting in %1 seconds"
msgstr[0] ""
"Instalante programarajn ĝisdatigojn kaj restartigante post 1 sekundo"
msgstr[1] ""
"Instalante programarajn ĝisdatigojn kaj restartigante post %1 sekundoj"

#: contents/logout/Logout.qml:248
#, kde-format
msgid "Restarting in 1 second"
msgid_plural "Restarting in %1 seconds"
msgstr[0] "Restartigante post 1 sekundo"
msgstr[1] "Restartigante post %1 sekundoj"

#: contents/logout/Logout.qml:250
#, kde-format
msgid "Shutting down in 1 second"
msgid_plural "Shutting down in %1 seconds"
msgstr[0] "Malŝaltante post 1 sekundo"
msgstr[1] "Malŝaltante post %1 sekundoj"

#: contents/logout/Logout.qml:252
#, kde-format
msgid "Logging out in 1 second"
msgid_plural "Logging out in %1 seconds"
msgstr[0] "Elsalutante post 1 sekundo"
msgstr[1] "Elsalutante post %1 sekundoj"

#: contents/logout/Logout.qml:263
#, kde-format
msgid "OK"
msgstr "bone"

#: contents/logout/Logout.qml:275
#, kde-format
msgid "Cancel"
msgstr "Nuligi"

#: contents/osd/OsdItem.qml:32
#, kde-format
msgctxt "Percentage value"
msgid "%1%"
msgstr "%1%"

#: contents/splash/Splash.qml:79
#, kde-format
msgctxt ""
"This is the first text the user sees while starting in the splash screen, "
"should be translated as something short, is a form that can be seen on a "
"product. Plasma is the project name so shouldn't be translated."
msgid "Plasma made by KDE"
msgstr "Plasmo farita de KDE"

#~ msgid "Switch to This Session"
#~ msgstr "Ŝanĝi al Ĉi tiu Seanco"

#~ msgid "Start New Session"
#~ msgstr "Komenci Novan Seancon"

#~ msgid "Back"
#~ msgstr "Reen"

#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgid "Battery at %1%"
#~ msgstr "Baterio ĉe %1%"

#~ msgctxt "Nobody logged in on that session"
#~ msgid "Unused"
#~ msgstr "Neuzata"

#~ msgctxt "User logged in on console number"
#~ msgid "TTY %1"
#~ msgstr "TTY %1"

#~ msgctxt "User logged in on console (X display number)"
#~ msgid "on TTY %1 (Display %2)"
#~ msgstr "ĉe TTY %1 (Ekrano %2)"

#~ msgctxt "Username (location)"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"
