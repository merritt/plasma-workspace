# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Steve Allewell <steve.allewell@gmail.com>, 2014, 2015, 2016, 2017, 2019, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-09 01:37+0000\n"
"PO-Revision-Date: 2022-09-18 12:09+0100\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English <kde-l10n-en_gb@kde.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.08.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Steve Allewell"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "steve.allewell@gmail.com"

#: main.cpp:57 view.cpp:49
#, kde-format
msgid "KRunner"
msgstr "KRunner"

#: main.cpp:57
#, kde-format
msgid "Run Command interface"
msgstr "Run Command interface"

#: main.cpp:63
#, kde-format
msgid "Use the clipboard contents as query for KRunner"
msgstr "Use the clipboard contents as query for KRunner"

#: main.cpp:64
#, kde-format
msgid "Start KRunner in the background, don't show it."
msgstr "Start KRunner in the background, don't show it."

#: main.cpp:65
#, kde-format
msgid "Replace an existing instance"
msgstr "Replace an existing instance"

#: main.cpp:66
#, kde-format
msgid "Show only results from the given plugin"
msgstr ""

#: main.cpp:67
#, kde-format
msgid "List available plugins"
msgstr ""

#: main.cpp:74
#, kde-format
msgid "The query to run, only used if -c is not provided"
msgstr "The query to run, only used if -c is not provided"

#: main.cpp:83
#, kde-format
msgctxt "Header for command line output"
msgid "Available KRunner plugins, pluginId"
msgstr ""

#: qml/RunCommand.qml:100
#, kde-format
msgid "Configure"
msgstr "Configure"

#: qml/RunCommand.qml:101
#, kde-format
msgid "Configure KRunner Behavior"
msgstr "Configure KRunner Behaviour"

#: qml/RunCommand.qml:104
#, kde-format
msgid "Configure KRunner…"
msgstr "Configure KRunner…"

#: qml/RunCommand.qml:117
#, kde-format
msgid "Showing only results from %1"
msgstr ""

#: qml/RunCommand.qml:131
#, kde-format
msgctxt "Textfield placeholder text, query specific KRunner plugin"
msgid "Search '%1'…"
msgstr "Search '%1'…"

#: qml/RunCommand.qml:132
#, kde-format
msgctxt "Textfield placeholder text"
msgid "Search…"
msgstr "Search…"

#: qml/RunCommand.qml:305 qml/RunCommand.qml:306 qml/RunCommand.qml:308
#, kde-format
msgid "Show Usage Help"
msgstr "Show Usage Help"

#: qml/RunCommand.qml:316
#, kde-format
msgid "Pin"
msgstr "Pin"

#: qml/RunCommand.qml:317
#, kde-format
msgid "Pin Search"
msgstr "Pin Search"

#: qml/RunCommand.qml:319
#, kde-format
msgid "Keep Open"
msgstr "Keep Open"

#: qml/RunCommand.qml:397 qml/RunCommand.qml:402
#, kde-format
msgid "Recent Queries"
msgstr "Recent Queries"

#: qml/RunCommand.qml:400
#, kde-format
msgid "Remove"
msgstr "Remove"

#~ msgid "in category recent queries"
#~ msgstr "in category recent queries"

#~ msgid "krunner"
#~ msgstr "krunner"

#~ msgid "Show KRunner"
#~ msgstr "Show KRunner"

#~ msgctxt "Name for krunner shortcuts category"
#~ msgid "KRunner"
#~ msgstr "KRunner"

#~ msgid "Run Command on clipboard contents"
#~ msgstr "Run Command on clipboard contents"

#~ msgid "Run Command"
#~ msgstr "Run Command"

#~ msgctxt "Name for krunner shortcuts category"
#~ msgid "Run Command"
#~ msgstr "Run Command"
