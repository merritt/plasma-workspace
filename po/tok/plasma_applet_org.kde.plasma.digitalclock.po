# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-19 01:35+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: tok\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: package/contents/config/config.qml:19
#, kde-format
msgid "Appearance"
msgstr ""

#: package/contents/config/config.qml:24
#, kde-format
msgid "Calendar"
msgstr ""

#: package/contents/config/config.qml:29
#: package/contents/ui/CalendarView.qml:426
#, kde-format
msgid "Time Zones"
msgstr ""

#: package/contents/ui/CalendarView.qml:142
#, kde-format
msgid "Events"
msgstr ""

#: package/contents/ui/CalendarView.qml:150
#, kde-format
msgctxt "@action:button Add event"
msgid "Add…"
msgstr ""

#: package/contents/ui/CalendarView.qml:154
#, kde-format
msgctxt "@info:tooltip"
msgid "Add a new event"
msgstr ""

#: package/contents/ui/CalendarView.qml:386
#, kde-format
msgid "No events for today"
msgstr ""

#: package/contents/ui/CalendarView.qml:387
#, kde-format
msgid "No events for this day"
msgstr ""

#: package/contents/ui/CalendarView.qml:436
#, kde-format
msgid "Switch…"
msgstr ""

#: package/contents/ui/CalendarView.qml:437
#: package/contents/ui/CalendarView.qml:440
#, kde-format
msgid "Switch to another timezone"
msgstr ""

#: package/contents/ui/configAppearance.qml:48
#, kde-format
msgid "Information:"
msgstr ""

#: package/contents/ui/configAppearance.qml:52
#, kde-format
msgid "Show date"
msgstr ""

#: package/contents/ui/configAppearance.qml:60
#, kde-format
msgid "Adaptive location"
msgstr ""

#: package/contents/ui/configAppearance.qml:61
#, kde-format
msgid "Always beside time"
msgstr ""

#: package/contents/ui/configAppearance.qml:62
#, kde-format
msgid "Always below time"
msgstr ""

#: package/contents/ui/configAppearance.qml:70
#, kde-format
msgid "Show seconds:"
msgstr ""

#: package/contents/ui/configAppearance.qml:72
#, kde-format
msgctxt "@option:check"
msgid "Never"
msgstr ""

#: package/contents/ui/configAppearance.qml:73
#, kde-format
msgctxt "@option:check"
msgid "Only in the tooltip"
msgstr ""

#: package/contents/ui/configAppearance.qml:74
#: package/contents/ui/configAppearance.qml:94
#, kde-format
msgid "Always"
msgstr ""

#: package/contents/ui/configAppearance.qml:84
#, kde-format
msgid "Show time zone:"
msgstr ""

#: package/contents/ui/configAppearance.qml:89
#, kde-format
msgid "Only when different from local time zone"
msgstr ""

#: package/contents/ui/configAppearance.qml:103
#, kde-format
msgid "Display time zone as:"
msgstr ""

#: package/contents/ui/configAppearance.qml:108
#, kde-format
msgid "Code"
msgstr ""

#: package/contents/ui/configAppearance.qml:109
#, kde-format
msgid "City"
msgstr ""

#: package/contents/ui/configAppearance.qml:110
#, kde-format
msgid "Offset from UTC time"
msgstr ""

#: package/contents/ui/configAppearance.qml:122
#, kde-format
msgid "Time display:"
msgstr ""

#: package/contents/ui/configAppearance.qml:127
#, kde-format
msgid "12-Hour"
msgstr ""

#: package/contents/ui/configAppearance.qml:128
#: package/contents/ui/configCalendar.qml:51
#, kde-format
msgid "Use Region Defaults"
msgstr ""

#: package/contents/ui/configAppearance.qml:129
#, kde-format
msgid "24-Hour"
msgstr ""

#: package/contents/ui/configAppearance.qml:136
#, kde-format
msgid "Change Regional Settings…"
msgstr ""

#: package/contents/ui/configAppearance.qml:147
#, kde-format
msgid "Date format:"
msgstr ""

#: package/contents/ui/configAppearance.qml:155
#, kde-format
msgid "Long Date"
msgstr ""

#: package/contents/ui/configAppearance.qml:160
#, kde-format
msgid "Short Date"
msgstr ""

#: package/contents/ui/configAppearance.qml:165
#, kde-format
msgid "ISO Date"
msgstr ""

#: package/contents/ui/configAppearance.qml:170
#, kde-format
msgctxt "custom date format"
msgid "Custom"
msgstr ""

#: package/contents/ui/configAppearance.qml:205
#, kde-format
msgid ""
"<a href=\"https://doc.qt.io/qt-6/qml-qtqml-qt.html#formatDateTime-method"
"\">Time Format Documentation</a>"
msgstr ""

#: package/contents/ui/configAppearance.qml:229
#, kde-format
msgctxt "@label:group"
msgid "Text display:"
msgstr ""

#: package/contents/ui/configAppearance.qml:231
#, kde-format
msgctxt "@option:radio"
msgid "Automatic"
msgstr ""

#: package/contents/ui/configAppearance.qml:235
#, kde-format
msgctxt "@label"
msgid ""
"Text will follow the system font and expand to fill the available space."
msgstr ""

#: package/contents/ui/configAppearance.qml:244
#, kde-format
msgctxt "@option:radio setting for manually configuring the font settings"
msgid "Manual"
msgstr ""

#: package/contents/ui/configAppearance.qml:254
#, kde-format
msgctxt "@action:button"
msgid "Choose Style…"
msgstr ""

#: package/contents/ui/configAppearance.qml:267
#, kde-format
msgctxt "@info %1 is the font size, %2 is the font family"
msgid "%1pt %2"
msgstr ""

#: package/contents/ui/configAppearance.qml:274
#, kde-format
msgctxt "@title:window"
msgid "Choose a Font"
msgstr ""

#: package/contents/ui/configCalendar.qml:38
#, kde-format
msgid "General:"
msgstr ""

#: package/contents/ui/configCalendar.qml:39
#, kde-format
msgid "Show week numbers"
msgstr ""

#: package/contents/ui/configCalendar.qml:44
#, kde-format
msgid "First day of week:"
msgstr ""

#: package/contents/ui/configCalendar.qml:65
#, kde-format
msgid "Available Plugins:"
msgstr ""

#: package/contents/ui/configTimeZones.qml:42
#, kde-format
msgid ""
"Tip: if you travel frequently, add your home time zone to this list. It will "
"only appear when you change the systemwide time zone to something else."
msgstr ""

#: package/contents/ui/configTimeZones.qml:78
#, kde-format
msgid "Clock is currently using this time zone"
msgstr ""

#: package/contents/ui/configTimeZones.qml:80
#, kde-format
msgctxt ""
"@label This list item shows a time zone city name that is identical to the "
"local time zone's city, and will be hidden in the timezone display in the "
"plasmoid's popup"
msgid "Hidden while this is the local time zone's city"
msgstr ""

#: package/contents/ui/configTimeZones.qml:105
#, kde-format
msgid "Switch Systemwide Time Zone…"
msgstr ""

#: package/contents/ui/configTimeZones.qml:117
#, kde-format
msgid "Remove this time zone"
msgstr ""

#: package/contents/ui/configTimeZones.qml:127
#, kde-format
msgid "Systemwide Time Zone"
msgstr ""

#: package/contents/ui/configTimeZones.qml:127
#, kde-format
msgid "Additional Time Zones"
msgstr ""

#: package/contents/ui/configTimeZones.qml:140
#, kde-format
msgid ""
"Add more time zones to display all of them in the applet's pop-up, or use "
"one of them for the clock itself"
msgstr ""

#: package/contents/ui/configTimeZones.qml:149
#, kde-format
msgid "Add Time Zones…"
msgstr ""

#: package/contents/ui/configTimeZones.qml:159
#, kde-format
msgid "Switch displayed time zone by scrolling over clock applet"
msgstr ""

#: package/contents/ui/configTimeZones.qml:166
#, kde-format
msgid ""
"Using this feature does not change the systemwide time zone. When you "
"travel, switch the systemwide time zone instead."
msgstr ""

#: package/contents/ui/configTimeZones.qml:188
#, kde-format
msgid "Add More Timezones"
msgstr ""

#: package/contents/ui/configTimeZones.qml:199
#, kde-format
msgid ""
"At least one time zone needs to be enabled. Your local timezone was enabled "
"automatically."
msgstr ""

#: package/contents/ui/configTimeZones.qml:235
#, kde-format
msgid "%1, %2 (%3)"
msgstr ""

#: package/contents/ui/configTimeZones.qml:237
#, kde-format
msgid "%1, %2"
msgstr ""

#: package/contents/ui/main.qml:148
#, kde-format
msgid "Copy to Clipboard"
msgstr ""

#: package/contents/ui/main.qml:152
#, kde-format
msgid "Adjust Date and Time…"
msgstr ""

#: package/contents/ui/main.qml:158
#, kde-format
msgid "Set Time Format…"
msgstr ""

#: package/contents/ui/Tooltip.qml:31
#, kde-format
msgctxt "@info:tooltip %1 is a localized long date"
msgid "Today is %1"
msgstr ""

#: package/contents/ui/Tooltip.qml:133
#, kde-format
msgctxt "@label %1 is a city or time zone name"
msgid "%1:"
msgstr ""

#: plugin/clipboardmenu.cpp:110
#, kde-format
msgid "Other Calendars"
msgstr ""

#: plugin/clipboardmenu.cpp:118
#, kde-format
msgctxt "unix timestamp (seconds since 1.1.1970)"
msgid "%1 (UNIX Time)"
msgstr ""

#: plugin/clipboardmenu.cpp:121
#, kde-format
msgctxt "for astronomers (days and decimals since ~7000 years ago)"
msgid "%1 (Julian Date)"
msgstr ""

#: plugin/timezonemodel.cpp:146
#, kde-format
msgctxt "This means \"Local Timezone\""
msgid "Local"
msgstr ""

#: plugin/timezonemodel.cpp:148
#, kde-format
msgid "System's local time zone"
msgstr ""
